#!/usr/bin/env bash

alias apt-get="apt-get -Vy"

chrome_version="google-chrome-stable_current_amd64.deb"
dropbox_version="dropbox_2015.10.28_amd64.deb"
intellj_version="ideaIU-2016.3.3"
nvidia_version="nvidia-375"
pycharm_version="pycharm-professional-2016.3.2"
python_version="3.6.0"
webstorm_version="WebStorm-2016.3.2"

# Check if running as priviledged user.
if [ $(id -u) -ne 0 ]; then
    echo "Error. You must execute this script with sudo."
    exit 1
fi

# Sanity check
apt-get install wget tar

apt-get update
apt-get upgrade

echo "Installing Python ${python_version}..."
wget https://www.python.org/ftp/python/${python_version}/Python-${python_version}.tgz
tar -zxf Python-${python_version}.tgz
if [ -d Python-${python_version} ]; then
    cd Python-${python_version}
    sh configure && make && make install
    if  [ $? -eq 0 ]; then
        echo "Python ${python_version} installed successfully."
    else
        echo "Errors during installation of python ${python_version}. Exit status: $?."
    fi
    cd ..
    rm -r Python-${python_version}
fi
rm Python-${python_version}.tgz

echo "Updating pip and installing python packages..."
pip3 install -U pip
pip3 install -U beautifulsoup4 ipython numpy matplotlib pytest requests scikit-learn scipy tensorflow Theano
if  [ $? -eq 0 ]; then
    echo "Python packages installed successfully."
else
    echo "Errors during installation of python packages. Exit status: $?."
    echo "Let me guess... matplotlib."
fi

echo "Installing golang..."
apt-get install golang
if  [ $? -eq 0 ]; then
    echo "Package golang installed successfully."
else
    echo "Errors during installation of golang package. Exit status: $?."
fi

echo "Installing lua 5.3..."
apt-get install lua5.3
if  [ $? -eq 0 ]; then
    echo "Package lua installed successfully."
else
    echo "Errors during installation of golang package. Exit status: $?."
fi

echo "Installing VIM..."
apt-get install vim
if  [ $? -eq 0 ]; then
    echo "Package vim installed successfully."
else
    echo "Errors during installation of vim package. Exit status: $?."
fi

echo "Adding NVIDIA drivers PPA and installing $nvidia_version package..."
add-apt-repository -V ppa:graphics-drivers/ppa
apt-get update
apt-get install ${nvidia_version}
if  [ $? -eq 0 ]; then
    echo "Package $nvidia_version installed successfully."
else
    echo "Errors during installation of $nvidia_version. Exit status: $?."
fi

echo "Installing Google Chrome..."
wget https://dl.google.com/linux/direct/${chrome_version}
dpkg -i ${chrome_version}
if  [ $? -eq 0 ]; then
    version=$(google-chrome-stable --version | cut -d " " -f 3)
    echo "Google Chrome installed successfully with version $version."
else
    echo "Errors during installation of Google Chrome. Exit status: $?."
fi
if [ -f ${chrome_version} ]; then
    rm ${chrome_version}
fi

echo "Installing VLC..."
apt-get install vlc
if  [ $? -eq 0 ]; then
    echo "Package VLC installed successfully."
else
    echo "Errors during installation of VLC. Exit status: $?."
fi

echo "Installing qbittorrent..."
apt-get install qbittorrent
if  [ $? -eq 0 ]; then
    echo "Package qbittorrent installed successfully."
else
    echo "Errors during installation of qbittorrent. Exit status: $?."
fi

echo "Adding qnapi PPA and installing qnapi package..."
sudo add-apt-repository ppa:krzemin/qnapi
if  [ $? -ne 0 ]; then
    echo "Errors during adding of qnapi PPA. Exit status: $?."
fi
sudo apt-get update
sudo apt-get install qnapi
if  [ $? -eq 0 ]; then
    echo "Package qnapi installed successfully."
else
    echo "Errors during installation of qnapi. Exit status: $?."
fi

echo "Adding Numix theme..."
add-apt-repository ppa:numix/ppa
if  [ $? -ne 0 ]; then
    echo "Errors during adding of Numix PPA. Exit status: $?."
fi
sudo apt-get update
sudo apt-get install numix-gtk-theme numix-icon-theme-circle
if  [ $? -eq 0 ]; then
    echo "Package with numix theme installed successfully."
else
    echo "Errors during installation of numix theme. Exit status: $?."
fi

echo "Installing Unity Tweak Tool..."
apt-get install unity-tweak-tool
if  [ $? -eq 0 ]; then
    echo "Package unity-tweak-tool installed successfully."
else
    echo "Errors during installation of unity-tweak-tool. Exit status: $?."
fi

pycharm_package=${pycharm_version}".tar.gz"
echo "Installing Pycharm..."
wget https://download.jetbrains.com/python/${pycharm_package}
tar -zxf ${pycharm_package}
mv ${pycharm_version} /opt/
mv /opt/${pycharm_version} /opt/pycharm
chown --recursive maciek:maciek /opt/pycharm
if  [ $? -eq 0 ]; then
    echo "Pycharm installed correctly."
else
    echo "Errors during installation of Pycharm. Exit status: $?."
fi
if [ -f ${pycharm_package} ]; then
    rm ${pycharm_package}
fi

intellj_package=${intellj_version}".tar.gz"
echo "Installing Intellj Idea..."
wget https://download.jetbrains.com/idea/${intellj_package}
tar -zxf ${intellj_package}
mv ${intellj_version} /opt/
mv /opt/${intellj_version} /opt/intellj
chown --recursive maciek:maciek /opt/intellj
if  [ $? -eq 0 ]; then
    echo "Intellj Idea installed correctly."
else
    echo "Errors during installation of Intellj Idea. Exit status: $?."
fi
if [ -f ${intellj_package} ]; then
    rm ${intellj_package}
fi

webstorm_package=${webstorm_version}".tar.gz"
echo "Installing Webstorm..."
wget https://download.jetbrains.com/webstorm/${webstorm_package}
tar -zxf ${webstorm_package}
mv ${webstorm_version} /opt/
mv /opt/${webstorm_version} /opt/webstorm
chown --recursive maciek:maciek /opt/webstorm
if  [ $? -eq 0 ]; then
    echo "Webstorm installed correctly."
else
    echo "Errors during installation of Webstorm. Exit status: $?."
fi
if [ -f ${webstorm_package} ]; then
    rm ${webstorm_package}
fi

echo "Installing TeX Live..."
apt-get install texlive-full
if  [ $? -eq 0 ]; then
    echo "Package TeX Live installed successfully."
else
    echo "Errors during installation of TeX Live. Exit status: $?."
fi

echo "Installing Texmaker..."
apt-get install texmaker
if  [ $? -eq 0 ]; then
    echo "Package Texmaker installed successfully."
else
    echo "Errors during installation of Texmaker. Exit status: $?."
fi

echo "Installing Dropbox..."
wget https://www.dropbox.com/download?dl=packages/ubuntu/${dropbox_version}
dpkg -i ${dropbox_version}
if  [ $? -eq 0 ]; then
    echo "Package Dropbox installed successfully."
else
    echo "Errors during installation of Dropbox. Exit status: $?."
fi
if [ -f ${dropbox_version} ]; then
    rm ${dropbox_version}
fi

echo "End of the script."
